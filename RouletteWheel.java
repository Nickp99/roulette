import java.util.Random;
public class RouletteWheel{
   private Random rand;
   private int lastSpin;

   public RouletteWheel(){
    this.rand = new Random();
   }
//stores the spin value into the int
public void spin(){
    this.lastSpin = this.rand.nextInt(37); 
}

public int getValue(){
    return this.lastSpin;
}

}
